/*
 * main.c: entry point for audiopic
 * Creation date: Sat Jun 16 10:32:54 2018
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>

#include <math.h>
#include <audiopic.h>
#include <sndfile.h>

#define SNDFILE_SAVE_FORMAT    SF_FORMAT_WAV | SF_FORMAT_PCM_16
#define SNDFILE_BYTES_PER_SAMP 2

PRIVATE void
progress(audiopic_t *ap, float progress, void *private)
{
  fprintf(stderr, "%3d%% completed\r", (int) ceilf(100 * progress));
}

PRIVATE BOOL
save_to_wav(const audiopic_t *ap, const char *wavfile)
{
  SNDFILE *sf = NULL;
  SF_INFO si;
  size_t len;

  BOOL ok = FALSE;

  memset(&si, 0, sizeof (SF_INFO));

  si.channels = 1;
  si.format = SNDFILE_SAVE_FORMAT;
  si.samplerate = audiopic_get_samp_rate(ap);

  if ((sf = sf_open(wavfile, SFM_WRITE, &si)) == NULL) {
    fprintf(stderr, "save_to_wav: failed to open `%s' for writing\n", wavfile);
    goto done;
  }

  len = audiopic_get_signal_length(ap);
  if (sf_write_float(sf, audiopic_get_signal_data(ap), len) != len) {
    fprintf(stderr, "save_to_wav: failed to save %d samples\n", len);
    goto done;
  }

  ok = TRUE;

done:
  if (sf != NULL)
    sf_close(sf);

  return ok;
}

void
banner(void)
{
  fprintf(stderr, "     _______\n");
  fprintf(stderr, "    (__,-,  \\\n");
  fprintf(stderr, "        / /\\ \\ AudioPic - Try to be as awesome as Aphex Twin,\n");
  fprintf(stderr, "       /,_) \\ \\ now for the whole family.\n");
  fprintf(stderr, "      (/      \\\\\n");
  fprintf(stderr, "               \\)\n\n");

  fprintf(stderr, "(c) BatchDrake 2018 - http://actinid.org\n");
  fprintf(stderr, "Copyrighted but free, under the terms of the GPL3 license\n");
}

int
main(int argc, char *argv[], char *envp[])
{
  FILE *fp = NULL;
  int ret = EXIT_FAILURE;
  audiopic_t *ap = NULL;
  struct audiopic_params params = audiopic_params_INITIALIZER;

  params.progress_func = progress;

  banner();

  if (argc != 3) {
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "\t  %s picture.jpg output.wav\n", argv[0]);
    goto done;
  }

  if ((fp = fopen(argv[1], "rb")) == NULL) {
    fprintf(
        stderr,
        "%s: failed to open `%s': %s\n",
        argv[0],
        argv[1],
        strerror (errno));
    goto done;
  }

  TRYCATCH(audiopic_log_composer_register(), goto done);

  TRYCATCH(audiopic_params_from_jpeg(&params, fp), goto done);

  fclose(fp);
  fp = NULL;

  if ((ap = audiopic_new(&params)) == NULL) {
    fprintf(stderr,"%s: failed to create audiopic object\n", argv[0]);
    goto done;
  }

  fprintf(
      stderr,
      "%s: input image is %dx%d\n",
      argv[0],
      params.width,
      params.height);
  fprintf(
      stderr,
      "%s: generating %g seconds of audio from %g Hz to %g Hz\n",
      argv[0],
      params.T,
      params.f0,
      params.f0 + params.bandwidth);
  fprintf(
      stderr,
      "%s: audio quality: %d Hz\n",
      argv[0],
      params.samp_rate);

  fprintf(
      stderr,
      "%s: output file will be roughly %g MiB in size\n",
      argv[0],
      SNDFILE_BYTES_PER_SAMP * audiopic_get_signal_length(ap) / (1024. * 1024.));
  fprintf(stderr, "%s: composing...\n", argv[0]);
  TRYCATCH(audiopic_compose_all(ap), goto done);

  fprintf(stderr, "%s: dumping raw data to %s...\n", argv[0], argv[2]);

  if ((fp = fopen(argv[2], "wb")) == NULL) {
    fprintf(stderr, "%s: failed to save file: %s\n", argv[0], strerror(errno));
    goto done;
  }

  if (!save_to_wav(ap, argv[2])) {
    fprintf(stderr, "%s: save failed\n", argv[0]);
    goto done;
  }

  ret = EXIT_SUCCESS;

done:
  if (ap != NULL)
    audiopic_destroy(ap);

  audiopic_params_free(&params);
  if (fp != NULL)
    fclose(fp);

  return ret;
}

