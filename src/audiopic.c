/*

  Copyright (C) 2018 Gonzalo José Carracedo Carballal

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/>

*/

#include <assert.h>
#include <string.h>
#include <stdlib.h>

#include "audiopic.h"

#include <math.h>

PTR_LIST(PRIVATE const struct audiopic_composer, composer);

const struct audiopic_composer *
audiopic_composer_lookup(const char *name)
{
  unsigned int i;

  for (i = 0; i < composer_count; ++i)
    if (strcmp(composer_list[i]->name, name) == 0)
      return composer_list[i];

  return NULL;
}

BOOL
audiopic_composer_register(const struct audiopic_composer *composer)
{
  assert(composer != NULL);
  assert(composer->name != NULL);
  assert(composer->init != NULL);
  assert(composer->compose != NULL);
  assert(audiopic_composer_lookup(composer->name) == NULL);

  TRYCATCH(
      PTR_LIST_APPEND_CHECK(composer, (void *) composer) != -1,
      return FALSE);

  return TRUE;
}

void
audiopic_params_free(struct audiopic_params *params)
{
  unsigned int i;

  if (params->buffer != NULL) {
    for (i = 0; i < params->height; ++i)
      if (params->buffer[i] != NULL)
        free(params->buffer[i]);

    free(params->buffer);
  }

  params->buffer = NULL;
  params->width = 0;
  params->height = 0;
}

void
audiopic_destroy(audiopic_t *ap)
{
  if (ap->private != NULL)
    if (ap->composer->dispose != NULL)
      (ap->composer->dispose) (ap->private);

  if (ap->signal != NULL)
    free(ap->signal);

  free(ap);
}

audiopic_t *
audiopic_new(const struct audiopic_params *params)
{
  audiopic_t *new = NULL;
  size_t len;
  const char *composer_name;
  const struct audiopic_composer *composer;

  TRYCATCH(params->f0 > 0, goto fail);
  TRYCATCH(params->bandwidth > 0, goto fail);
  TRYCATCH(params->T > 0, goto fail);
  TRYCATCH(params->width > 1, goto fail);
  TRYCATCH(params->height > 1, goto fail);
  TRYCATCH(params->buffer != NULL, goto fail);

  TRYCATCH(
      params->samp_rate >= 2 * (params->f0 + params->bandwidth),
      goto fail);

  if ((len = ceilf(params->T * params->samp_rate)) > MAX_SAMPLES) {
    fprintf(stderr, "audiopic_new: too many samples!\n");
    fprintf(
        stderr,
        "audopic_new: max length for this sample rate is %g sec\n",
        (float) MAX_SAMPLES / (float) params->samp_rate);
    goto fail;
  }

  if ((composer_name = params->composer) == NULL)
    composer_name = "log";

  if ((composer = audiopic_composer_lookup(composer_name)) == NULL) {
    fprintf(stderr, "audiopic_new: no such composer `%s'\n", composer_name);
    goto fail;
  }

  TRYCATCH(new = calloc(1, sizeof(audiopic_t)), goto fail);

  new->composer = composer;
  new->params = *params;
  new->len = len;
  new->amplitude = 1. / params->height;
  new->dT = 1. / params->samp_rate;

  /* Scale factor, converts from signal sample index to width coordinate */
  new->propT = (float) (params->width - 1) / (float) (len - 1);

  /* Allocate len + 1 for interpolation boundaries */
  TRYCATCH(new->signal = calloc(len + 1, sizeof(float)), goto fail);

  TRYCATCH(new->private = (composer->init) (new), goto fail);

  return new;

fail:
  if (new != NULL)
    audiopic_destroy(new);

  return NULL;
}

BOOL
audiopic_compose_all(audiopic_t *ap)
{
  unsigned int i;
  float ff;

  for (i = 0; i < ap->params.height; ++i) {
    ff = (float) i / (float) (ap->params.height - 1);
    if (ap->params.progress_func != NULL)
      if (i % 50 == 0 || i == ap->params.height - 1)
        (ap->params.progress_func) (ap, ff, ap->params.private);

    TRYCATCH(
        (ap->composer->compose)(
            ap->private,
            ff,
            ap->params.buffer[i],
            ap->params.width),
        return FALSE);
  }

  return TRUE;
}

