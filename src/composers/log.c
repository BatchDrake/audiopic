/*

  Copyright (C) 2018 Gonzalo José Carracedo Carballal

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/>

*/

#include "audiopic.h"
#include <math.h>

PRIVATE void *
audiopic_composer_log_init(audiopic_t *ap)
{
  return (void *) ap;
}

PRIVATE BOOL
audiopic_composer_log_compose(
    void *private,
    float ff,
    const float *line,
    size_t len)
{
  float freq;
  float phi = 0;
  float delta_phi;
  float A;
  float jf, scale, t;
  unsigned int i, j;

  const audiopic_t *ap = (const audiopic_t *) private;

  /*
   * The idea behind this is that the function that relates each vertical
   * coordinate to the logarithm of the frequency is a rect:
   *
   * y_0 = ln(f_0)
   * y_n = ln(f_0 + bw)
   *
   * Therefore, we have:
   *
   * ln(f) = m * y + b = (ln((f_0 + bw)/f0)) * y / y_n + ln(f_0)
   *
   * Where y / y_n is the "ff" parameter passed to the "compose" method. Thus:
   *
   * f = f_0 * ((f_0 + bw) / f0) ^ ff
   */

  freq = ap->params.f0 * powf(
          (ap->params.f0 + ap->params.bandwidth) / ap->params.f0,
          ff);

  delta_phi = 2 * M_PI * freq * ap->dT;

  for (i = 0; i < ap->len; ++i) {
    if (ap->len == len) {
      A = line[i];
    } else {
      jf = i * ap->propT;
      j = floor(jf);
      t = jf - j;
      A = (1. - t) * line[j] + t * line[j + 1];
    }

    ap->signal[i] += ap->amplitude * A * sinf(phi);

    phi += delta_phi;
    if (phi > 2 * M_PI)
      phi -= 2 * M_PI; /* Adjust every time to keep phase under control */
  }

  return TRUE;
}

BOOL
audiopic_log_composer_register(void)
{
  static struct audiopic_composer composer = {
      .name    = "log",
      .init    = audiopic_composer_log_init,
      .compose = audiopic_composer_log_compose,
      .dispose = NULL
  };

  TRYCATCH(audiopic_composer_register(&composer), return FALSE);

  return TRUE;
}

