/*

  Copyright (C) 2018 Gonzalo José Carracedo Carballal

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/>

*/

#ifndef _AUDIOPIC_H
#define _AUDIOPIC_H

#include <config.h> /* General compile-time configuration parameters */
#include <util.h> /* From util: Common utility library */
#include <stdlib.h>

#define MAX_SAMPLES 100000000 /* 100M samples should be enough */

#ifdef TRUE
#  undef TRUE
#endif

#ifdef FALSE
#  undef FALSE
#endif

#define FALSE 0
#define TRUE  1

#define BOOL int

#define PRIVATE static
#define INLINE static inline

#define TRYCATCH(expr, action)          \
  if (!(expr)) {                        \
    fprintf(                            \
      stderr,                           \
      "Exception in \"%s\" (%s:%d)\n",  \
      STRINGIFY(expr),                  \
      __FILE__,                         \
      __LINE__);                        \
      action;                           \
  }

struct audiopic;

struct audiopic_params {
  float f0;
  float bandwidth;
  float T;
  unsigned int samp_rate;

  const char *composer;

  unsigned int width;
  unsigned int height;

  float **buffer;

  void (*progress_func) (struct audiopic *, float progress, void *private);
  void *private;
};

#define audiopic_params_INITIALIZER     \
{                                       \
  55.0, /* f0 */                        \
  4000.0, /* bandwidth */               \
  30.0, /* T */                         \
  44100, /* samp_rate */                \
  NULL, /* composer */                  \
  0, /* width */                        \
  0, /* height */                       \
  NULL, /* buffer */                    \
  NULL, /* progress_func */             \
  NULL, /* private */                   \
}

struct audiopic_composer {
  const char *name;

  void *(*init) (struct audiopic *ap);
  BOOL (*compose) (
      void *private,
      float ff,
      const float *data,
      size_t len);
  void (*dispose) (void *private);
};

struct audiopic {
  struct audiopic_params params;
  const struct audiopic_composer *composer;

  float amplitude;
  float *signal;
  float dT;
  float propT;
  void *private;
  size_t len;
};

typedef struct audiopic audiopic_t;

INLINE const float *
audiopic_get_signal_data(const audiopic_t *audiopic)
{
  return audiopic->signal;
}

INLINE size_t
audiopic_get_signal_length(const audiopic_t *audiopic)
{
  return audiopic->len;
}

INLINE unsigned int
audiopic_get_samp_rate(const audiopic_t *audiopic)
{
  return audiopic->params.samp_rate;
}

BOOL audiopic_composer_register(const struct audiopic_composer *composer);
BOOL audiopic_log_composer_register(void);

void audiopic_destroy(audiopic_t *ap);
audiopic_t *audiopic_new(const struct audiopic_params *params);
BOOL audiopic_compose_all(audiopic_t *ap);

BOOL audiopic_params_from_jpeg(struct audiopic_params *params, FILE *input);
void audiopic_params_free(struct audiopic_params *params);

#endif /* _AUDIOPIC_H */
