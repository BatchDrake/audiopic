/*

  Copyright (C) 2018 Gonzalo José Carracedo Carballal

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/>

*/

#include <stdlib.h>

#include "audiopic.h"
#include "jpeglib.h"

#define MAX3(R, G, B) MAX(R, MAX(G, B))
#define MIN3(R, G, B) MIN(R, MIN(G, B))

BOOL
audiopic_params_from_jpeg(struct audiopic_params *params, FILE *input)
{
  struct jpeg_decompress_struct cinfo;
  struct jpeg_error_mgr jerr;
  int i, j;
  JSAMPROW row_pointer = NULL;
  BOOL ok = FALSE;
  BOOL decomp = FALSE;

  params->buffer = NULL;
  params->width = 0;
  params->height = 0;

  cinfo.err = jpeg_std_error(&jerr);
  jpeg_create_decompress(&cinfo);
  jpeg_stdio_src(&cinfo, input);
  jpeg_read_header(&cinfo, TRUE);

  /* Decompress image */
  if (cinfo.num_components == 3)
    cinfo.out_color_space = JCS_RGB;
  else if (cinfo.num_components != 1) {
    fprintf(
        stderr,
        "audiopic_params_from_jpeg: unsupported component number %d\n",
        cinfo.num_components);
    goto done;
  }

  jpeg_start_decompress(&cinfo);
  decomp = TRUE;

  /* Save image size in params */
  params->width = cinfo.output_width;
  params->height = cinfo.output_height;

  /* Allocate scanlines */
  TRYCATCH(params->buffer = calloc(params->height, sizeof(float *)), goto done);

  for (i = 0; i < params->height; ++i)
    TRYCATCH(
        params->buffer[i] = malloc(params->width * sizeof(float)),
        goto done);

  TRYCATCH(
      row_pointer = malloc(cinfo.output_width * cinfo.output_components),
      goto done);

  for (j = params->height - 1; j >= 0; --j) {
    jpeg_read_scanlines(&cinfo, &row_pointer, 1);

    if (cinfo.output_components == 1) { /* Grayscale */
      for (i = 0; i < params->width; ++i) {
        params->buffer[j][i] = row_pointer[i] / 255.;
      }
    } else {
      for (i = 0; i < params->width; ++i) { /* RGB */
        params->buffer[j][i] =
            (MAX3(
                row_pointer[3 * i + 0],
                row_pointer[3 * i + 1],
                row_pointer[3 * i + 2]) +
             MIN3(
                 row_pointer[3 * i + 0],
                 row_pointer[3 * i + 1],
                 row_pointer[3 * i + 2])) / 510.0;
      }
    }

  }

  ok = TRUE;

done:
  if (decomp) {
    jpeg_finish_decompress(&cinfo);
    jpeg_destroy_decompress(&cinfo);
  }

  if (!ok)
    audiopic_params_free(params);

  if (row_pointer != NULL)
    free(row_pointer);

  return ok;
}
